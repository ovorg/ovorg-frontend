import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import router from "./router";
import store from "./store";
import "./service-worker";
import "./plugins";
import "./can";
import "./axiosErrorHandler";

Vue.use(require("vue-shortkey"));

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount("#app");
