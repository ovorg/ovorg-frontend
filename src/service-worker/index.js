import registerServiceWorker from "./registerServiceWorker.js";

if (process.env.NODE_ENV === "production" || true) {
  registerServiceWorker();
}
