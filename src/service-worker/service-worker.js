const CACHE_NAME = "ovorg-frontend";

workbox.core.setCacheNameDetails({ prefix: CACHE_NAME });

workbox.precaching.precacheAndRoute(self.__precacheManifest);
workbox.routing.registerNavigationRoute("/index.html", {
  blacklist: [
    /.*\.css/,
    /.*\.(?:png|jpg|jpeg|svg|gif)/,
    /.*\.(?:js)(?:$|\?)/
  ]
});
workbox.core.clientsClaim();

self.addEventListener("message", e => {
  if (!e.data) {
    return;
  }

  switch (e.data) {
    case "SKIP_WAITING":
      self.skipWaiting();
      break;
    default:
      // NOOP
      break;
  }
});

/* Return true if the request should be handled
 * with the network then cache strategy.
 */
function useNetworkThenCacheStrategy(request) {
  return /\/api\/me$/.test(request.url);
}

function networkThenCache(request) {
  return caches.open(CACHE_NAME).then(function(cache) {
    return fetch(request)
      .then(function(response) {
        // Network request succeded. Store response in cache.
        cache.put(request, response.clone());
        return response;
      })
      .catch(function() {
        // Network request failed. Get response from cache.
        return cache.match(request);
      });
  });
}

self.addEventListener("fetch", function(event) {
  // Don't cache non GET requests.
  if (event.request.method != "GET") return;

  if (useNetworkThenCacheStrategy(event.request)) {
    event.respondWith(networkThenCache(event.request));
    return;
  }
});
