describe("OVOrg Login", () => {
  beforeEach(function() {
    cy.server({ force404: true });
  });
  it("loads the page", () => {
    cy.visit("/");
  });
  it("cookie consent working", () => {
    cy.visit("/");
    cy.get("[data-cy=cookie-title]").should("contain.text", "Cookie");
    cy.get("[data-cy=cookie-button]").click();
    cy.wait(500);
    cy.get("[data-cy=cookie-title]")
      .should("contain.text", "Cookie")
      .should("not.be.visible");
  });
  it("password reset working", () => {
    cy.route("POST", "http://ovorg.spa/api/request_password_reset", "OK").as(
      "apiRequestPasswordReset"
    );
    cy.readCookie();
    cy.visit("/");
    cy.get("[data-cy=login-reset]").click();
    cy.get("[data-cy=reset-button]").click();
    cy.get(".v-form > :nth-child(1)").should("contain.text", "Ungültig");
    cy.get("[data-cy=reset-mail]").type("test@test.de{enter}");
    cy.wait("@apiRequestPasswordReset")
      .its("request.body")
      .should("contain", { email: "test@test.de" });
  });
  it("login working", () => {
    cy.readCookie();
    cy.visit("/");
    cy.route("POST", "http://ovorg.spa/api/login", "fixture:login");
    cy.route("GET", "http://ovorg.spa/api/me", "fixture:me");
    cy.get("[data-cy=login-title]").should("contain.text", "Anmelden");
    cy.get("[data-cy=login-button]").click();
    cy.get(".v-form > :nth-child(1)").should("contain.text", "Benötigt");
    cy.get(".v-form > :nth-child(2)").should("contain.text", "Benötigt");
    cy.get("[data-cy=username]").type("Test");
    cy.get("[data-cy=password]").type("123");
    cy.get("[data-cy=login-button]").click();
  });
});
