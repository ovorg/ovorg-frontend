describe("OVOrg Home", () => {
  beforeEach(function() {
    cy.login();
    cy.route("GET", "http://ovorg.spa/api/users", "fixture:users/all");
  });
  it.only("Loads the page", () => {
    cy.visit("/helfer");
  });
  it("Form working", () => {
    cy.visit("/helfer");
    cy.route(
      "GET",
      "http://ovorg.spa/api/user_groups",
      "fixture:user_groups/all"
    );
    cy.route("GET", "http://ovorg.spa/api/divisions", "fixture:divisions/all");
    cy.route("GET", "http://ovorg.spa/api/positions", "fixture:positions/all");
    cy.route("POST", "http://ovorg.spa/api/users", "OK");
    cy.contains("Erstellen").click();
    cy.get("#thwin_id").type("12345678");
    cy.get("#first_name").type("Anna");
    cy.get("#last_name").type("Musterfrau");
    cy.get("#gender")
      .parent("div")
      .type("{downarrow}{downarrow}{enter}");
    cy.get("#birth")
      .parent("div")
      .type("{enter}");
    cy.contains("2019").click();
    cy.wait(200)
      .contains("Jan")
      .click();
    cy.wait(200)
      .contains("22")
      .click();
    cy.get("#division")
      .parent("div")
      .type("{downarrow}{enter}");
    cy.get("#position")
      .parent("div")
      .type("{downarrow}{enter}");
    cy.get("#user_group")
      .parent("div")
      .type("{downarrow}{enter}{esc}");
    cy.get("#mobile")
      .type("+49 1234 8765432")
      .should("have.value", "+4912348765432");
    cy.get("#phone")
      .type("+49 1234 8765432")
      .should("have.value", "+4912348765432");
    cy.get("#mail").type("test@test.de");
    cy.contains("Erstellen").click();
    cy.get("[data-cy=snackbar]").should("contain.text", "Erfolgreich");
  });
});
