// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add("login", () => {
  cy.fixture("login").then(json => {
    window.localStorage.setItem("accessToken", json.access_token);
    window.localStorage.setItem("refreshToken", json.refresh_token);
    window.localStorage.setItem("cookies", "false");
  });
  cy.fixture("me").then(json => {
    window.localStorage.setItem(
      "permissions",
      JSON.stringify(json.data.permissions)
    );
  });
  cy.server({ force404: true });
  cy.route("POST", "http://ovorg.spa/api/login", "fixture:login");
  cy.route("GET", "http://ovorg.spa/api/me", "fixture:me");
});

Cypress.Commands.add("readCookie", () => {
  window.localStorage.setItem("cookies", "false");
});
