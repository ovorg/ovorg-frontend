# OVOrg Frontend

This is the frontend for the OVOrg system. A management system adapted to the needs of the Federal Agency for Technical Relief.

## Requirements

OVOrg is build using Vue CLI. You can check the requirements of Vue CLI in their [documentation](https://cli.vuejs.org/guide/installation.html).<br>
You need a HTTP server to serve the application.<br>


## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## License

Copyright (C) 2020 Lukas Schneider<br>
Copyright (C) 2020 Andreas Roll<br>
Copyright (C) 2020 Oliver Friedel<br>

OVOrg Frontend is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.
