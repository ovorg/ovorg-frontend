@servers(['server' => "$ssh_user@$ssh_ip"])

@setup
    $release = date('Y.m.d.H.i.s');
    $app_dir = '~/ovorg-spa';
    if ($dev) {
        $app_dir = $app_dir.'-dev';
    }
    $release_dir = $app_dir.'/releases';
    $new_release_dir = $release_dir.'/'.$release;
    $prev_link = $app_dir.'/prev';
    $curr_link = $app_dir.'/current';

    $branch = isset($branch) ? $branch : 'master';

    $err_msg = "Error while deploying ";
    if ($dev) {
        $msg = "Staging frontend";
        $err_msg = $err_msg."staging frontend";
    } else {
        $msg = "Frontend";
        $err_msg = $err_msg."frontend";
    }
    $msg = $msg." has been deployed.";

    $branch_msg = "\nBranch: $branch";
    if ($commit) {
        $commit_msg = "\nCommit: $commit";
    } else {
        $commit_msg = "";
    }

    $error_message = "```\n".$err_msg.$branch_msg.$commit_msg."\n```";
    $success_message = "```\n".$msg.$branch_msg.$commit_msg."\n```";
@endsetup

# Create folder structure and copy .env
# Do not build the application
@story('init')
    create_dirs
    clone_repo
    create_env
    delete_release
@endstory

# Create and link a new release
@story('deploy')
    cd
    clone_repo
    link_env
    npm_install
    npm_audit_fix
    npm_build
    delete_prev
    create_prev
    publish
@endstory

@task('npm_install')
    echo "Installing dependencies..."
    cd {{ $new_release_dir }}
    npm install --production
@endtask

@task('npm_audit_fix')
    echo "npm audit fix"
    cd {{ $new_release_dir }}
    npm audit fix
@endtask

@task('npm_build')
    echo "Building application..."
    cd {{ $new_release_dir }}
    npm run build
@endtask

@task('publish')
    echo "Publishing application..."
    ln -nfs {{ $new_release_dir }} {{ $curr_link }}
@endtask

@task('link_env')
    echo "Linking .env..."
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env
@endtask

@task('delete_prev')
    if [ -d $(readlink -f {{ $prev_link }} ) ]; then
        rm -rf $(readlink -f {{ $prev_link }})
        echo "Old backup deleted."
    fi
@endtask

@task('create_prev')
    if [ -d $(readlink -f {{ $curr_link }} ) ]; then
        ln -nfs $(readlink -f {{ $curr_link }}) {{ $prev_link }}
        echo "Backup created."
    fi
@endtask

@task('cd')
    if [ -d {{ $app_dir }} ]; then
        cd {{ $app_dir }}
        echo "Deploying application..."
    else
        echo "Directory does not exist. Aborting..."
        echo "Hint: Run init task."
        false
    fi
@endtask

@task('create_dirs')
    if [ -d {{ $app_dir }} ]; then
        echo "App directory already exists. Aborting..."
        false
    else
        echo "Creating app directory..."
        mkdir -p -m=711 {{ $release_dir }}
    fi
@endtask

@task('clone_repo')
    echo "Cloning repository..."

    git clone {{ $repo_url }} --branch={{ $branch }} {{ $new_release_dir }}

    @if($commit)
        cd {{ $new_release_dir }}
        git reset --hard {{ $commit }}
    @endif
@endtask

@task('create_env')
    echo "Creating .env..."
    cp {{ $new_release_dir }}/.env.example {{ $app_dir }}/.env
@endtask

@task('delete_release')
    echo "Deleting repository..."
    rm -r {{ $new_release_dir }}
@endtask

@finished
    if(isset($discord_webhook)) {
        @discord($discord_webhook, $success_message)
    }
@endfinished

@error
    if(isset($discord_webhook)) {
        @discord($discord_webhook, $error_message)
    }
    exit(1);
@enderror
