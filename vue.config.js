module.exports = {
  runtimeCompiler: true,
  pwa: {
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      swSrc: "./src/service-worker/service-worker.js",
      swDest: "service-worker.js"
    }
  }
};
